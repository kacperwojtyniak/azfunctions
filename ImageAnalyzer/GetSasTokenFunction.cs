using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Net.Http;
using Microsoft.Azure.WebJobs.Extensions.SignalRService;

namespace ImageAnalyzer
{
    public static class GetSasToken
    {
        [FunctionName("GetSasToken")]
        public static IActionResult Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "get")] HttpRequest req,
            [Blob("images", FileAccess.ReadWrite, Connection = "storageAccount")] CloudBlobContainer container)
        {
            string requestedFormat = req.Query["format"];
            var blob = container.GetBlobReference(Guid.NewGuid().ToString() + requestedFormat);
            var policy = new SharedAccessBlobPolicy()
            {
                Permissions = SharedAccessBlobPermissions.Read | SharedAccessBlobPermissions.Write | SharedAccessBlobPermissions.Create,
                SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(10)
            };
            var sas = blob.GetSharedAccessSignature(policy);
            var result = blob.Uri + sas;

            return new OkObjectResult(result);
        }
    }
}
