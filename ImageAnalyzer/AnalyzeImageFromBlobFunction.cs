using ImageAnalyzer.Models;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.SignalRService;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ImageAnalyzer
{
    public static class AnalyzeImageFromBlobFunction
    {
        // Specify the features to return
        private static readonly List<VisualFeatureTypes> features =
            new List<VisualFeatureTypes>()
        {
            VisualFeatureTypes.Tags
        };


        [FunctionName("AnalyzeImageFromBlob")]
        public static async Task Run(
            [BlobTrigger("images", Connection = "storageAccount")]CloudBlockBlob imageBlob,
            [Table("AnalysisResults", Connection = "storageAccount")]CloudTable table,
            [SignalR(HubName = "imageAnalysis")]IAsyncCollector<SignalRMessage> signalRMessages)
        {
            var blobAccessPolicy = new SharedAccessBlobPolicy()
            {
                Permissions = SharedAccessBlobPermissions.Read,
                SharedAccessExpiryTime = DateTime.UtcNow.AddMinutes(5)
            };

            var sas = imageBlob.GetSharedAccessSignature(blobAccessPolicy);
            var imageUrl = imageBlob.Uri + sas;

            var computerVisionKey = System.Environment.GetEnvironmentVariable("COMPUTER_VISION_KEY");
            var computerVisionUrl = System.Environment.GetEnvironmentVariable("COMPUTER_VISION_URL");
            ComputerVisionClient computerVision = new ComputerVisionClient(
                new ApiKeyServiceClientCredentials(computerVisionKey),
                new System.Net.Http.DelegatingHandler[] { });

            computerVision.Endpoint = computerVisionUrl;


            var computerVisionResult = await computerVision.AnalyzeImageAsync(imageUrl, features);
            var tags = JsonConvert.SerializeObject(computerVisionResult.Tags.Select(t => t.Name));


            var tableRow = new AnalysisResult("Analyzed", imageBlob.Name, tags);
            TableOperation insertOrMergeOperation = TableOperation.Insert(tableRow);
            var res = await table.ExecuteAsync(insertOrMergeOperation);

            var msg = ((AnalysisResult)res.Result).Tags;
            await signalRMessages.AddAsync(new SignalRMessage()
            {
                Target = "analysisReceived",
                Arguments = new object[] { msg, imageBlob.Uri }
            });
        }

    }
}
